package yarr.rest;

import java.net.URL;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.app.ObjectContainer;
import yarr.feed.fetch.FeedFetcher;
import yarr.feed.update.FeedInfoMapper;
import yarr.feed.update.FeedItemUpdater;
import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.FeedItemDAO;
import yarr.stor.domain.Feed;
import yarr.stor.domain.FeedItem;

import com.sun.syndication.feed.synd.SyndFeed;

@Path("feeds")
public class FeedService
{
	// Logging object.
	public static final Logger logger = LoggerFactory.getLogger(FeedService.class);
	// Servlet context. Needed for authentication.
	@Context ServletContext context;
	private FeedDAO feedDAO = null;
	private FeedItemDAO feedItemDAO = null;
	
	/**
	 * Get the feed DAO and create it if it doesn't exist yet.
	 * @return
	 */
	private FeedDAO getFeedDAO()
	{
		if (feedDAO == null)
			feedDAO = ObjectContainer.getInstance().getFeedDAOFacade().getFeedDAO();
		return feedDAO;
	}

	/**
	 * Get the feed item DAO and create it if it doesn't exist yet.
	 * @return
	 */
	private FeedItemDAO getFeedItemDAO()
	{
		if (feedItemDAO == null)
			feedItemDAO = ObjectContainer.getInstance().getFeedDAOFacade().getFeedItemDAO();
		return feedItemDAO;
	}
	
	/**
	 * Get feed information.
	 * @param feedId
	 * @return
	 */
	@GET
	@Path("/{feedId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeedInfo(@PathParam("feedId") String feedId)
	{
		Feed feed = null;
		try
		{
			feed = getFeedDAO().getFeedById(feedId);
		}
		catch (RuntimeException e)
		{
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		if (feed == null || feed.getId() == null)
		{
			String message = "Feed with id " + feedId + " not found";
			logger.info(message);
			// Create a 404 (not found) response.
			return Response
					.status(404)
					.entity(message)
					.type(MediaType.TEXT_PLAIN)
					.build();
		}
		
		return Response
				.status(200)
				.entity(feed)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	/**
	 * Get a feed item.
	 * @param feedItemId
	 * @return
	 */
	@GET
	@Path("/feed_item/{feedItemId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeedItem(@PathParam("feedItemId") String feedItemId)
	{
		FeedItem feedItem = null;
		try
		{
			feedItem = getFeedItemDAO().getById(feedItemId);
		}
		catch (RuntimeException e)
		{
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
		
		if (feedItem == null)
		{
			String message = "Feed item with id " + feedItemId + " not found";
			logger.info(message);
			// Create a 404 (not found) response.
			return Response.status(404)
					.entity(message)
					.type(MediaType.TEXT_PLAIN)
					.build();
		}

		return Response.status(200)
				.entity(feedItem)
				.type(MediaType.APPLICATION_JSON)
				.build();
	}
	
	/**
	 * Get the feed items for a given feed id.
	 * @param feedId
	 * @param limit
	 * @param offset
	 * @return
	 */
	@GET
	@Path("/feed_items/{feedId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getItemsForFeed(@PathParam("feedId") String feedId, @QueryParam("max") String limit, 
			@QueryParam("offset") String offset)
	{
		Set<FeedItem> feedItems = getFeedItemDAO()
				.getFeedItemsForFeed(feedId, getIntFromString(limit), getIntFromString(offset));
		return Response.status(200)
				.entity(feedItems)
				.type(MediaType.APPLICATION_JSON + "; charset=UTF-8")
				.build();
	}

	/**
	 * Search in feed items.
	 * @param searchString The regexp to search with.
	 * @param feeds An (optional) array of feed ids to search in
	 * @param limit Limit results to this amount of entries
	 * @param offset Skip this number of entries.
	 * @return
	 */
	@GET
	@Path("/feed_items/search/{searchString}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchFeedItems(@PathParam("searchString") String searchString, @QueryParam("feed") List<String> feeds,
			@QueryParam("max") String limit, @QueryParam("offset") String offset)
	{
		Set<FeedItem> feedItems = getFeedItemDAO()
				.findInDescription(searchString, getIntFromString(limit), getIntFromString(offset), feeds);
		Response response = null;
		logger.debug("Feed items: {}; feeds: {}; search: {}", feedItems, feeds, searchString);
		if (feedItems == null || feedItems.size() == 0)
			response = Response.status(404)
				.entity("No items found")
				.type(MediaType.TEXT_PLAIN)
				.build();
		else
			response = Response.status(200)
				.entity(feedItems)
				.type(MediaType.APPLICATION_JSON + "; charset=UTF-8")
				.build();
		
		return response;
	}
	
	/**
	 * Add a feed to the database.
	 * @param url
	 * @return
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Feed addFeed(String url)
	{
		FeedDAO feedDAO = getFeedDAO(); 
		Feed existing = feedDAO.getFeedByFeedUrl(url);
		if (existing == null)
		{
			try
			{
				FeedFetcher fetcher = ObjectContainer.getInstance().getFeedFetcher();
				final SyndFeed feedInfo = fetcher.fetch(new URL(url));
				final Feed newFeed = FeedInfoMapper.syndFeedToFeed(feedInfo);
				newFeed.setFeedUrl(url);
				getFeedDAO().create(newFeed);
				logger.info("New feed from url " + url + " created with id " + newFeed.getId());
				// Fetch feed items in the background.
				Thread t = new Thread(new Runnable() 
				{
					@Override
					public void run()
					{
						logger.info("Fetching items for feed {}", newFeed.getId());
						FeedItemUpdater updater = new FeedItemUpdater(getFeedItemDAO());
						updater.updateFromSyndFeed(newFeed, feedInfo);
					}
				});
				t.start();
				
				return newFeed;
			}
			catch (Exception e)
			{
				logger.error("Feed retrieval of {} failed", url, e);
				throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
			}
		}
		
		return existing;
	}
	
	/**
	 * This is going to be necessary when handling Access-Control-Allow-Origin checks.
	 * @param servletResponse
	 * @return
	 */
	@OPTIONS
	@Path("/{feedId}")
	public Response handleOptions(@Context HttpServletResponse servletResponse)
	{
		// http://jersey.576304.n2.nabble.com/Access-Control-Allow-Origin-td6060693.html
		return Response.ok()
			.header("Allow-Control-Allow-Methods", "POST,GET,PUT,OPTIONS")
			.header("Access-Control-Allow-Origin", "*")
			.build();
	}
	
	/**
	 * Get an int from a string.
	 * @param i
	 * @return
	 */
	private int getIntFromString(String i)
	{
		try
		{
			if (i != null)
				return Integer.parseInt(i);
		}
		catch (Exception e)
		{
			logger.warn("Invalid int given: {}", i, e);
		}
		
		return 0;
		
	}
}
