/**
 * 
 * File created at 3 apr. 2013 22:25:30 by Johan Mulder <johan@mulder.net>
 */
package yarr.app.web;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import yarr.app.ObjectContainer;
import yarr.feed.fetch.FetchJob;

/**
 * Web application initialization class.
 * @author Johan Mulder <johan@mulder.net>
 */
public class ContextListener implements ServletContextListener
{
	// Logger object.
	private static final Logger logger = LoggerFactory.getLogger(ContextListener.class);
	
	private void initObjectContainer()
	{
		// Initialize all objects necessary for various services to function properly.
		logger.info("Initializing ObjectContainer");
		ObjectContainer.getInstance();
		logger.info("Initialization of ObjectContainer done");
		
	}

	/**
	 * Start the job scheduler.
	 */
	private void initJobScheduler()
	{
		logger.info("Starting job scheduler");
		try
		{
			Scheduler scheduler = ObjectContainer.getInstance().getJobScheduler();
			scheduler.start();
			JobDetail job = org.quartz.JobBuilder.newJob(FetchJob.class)
					.withIdentity("feedfetcher")
					.build();
			Trigger trigger = org.quartz.TriggerBuilder.newTrigger()
					.withIdentity("fetchtrigger")
					.startNow()
					.withSchedule(org.quartz.SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(10).repeatForever())
					.build();
			scheduler.scheduleJob(job, trigger);
		}
		catch (SchedulerException e)
		{
			logger.error("Scheduler initialization failed {}", e);
			throw new RuntimeException(e);
		}
		logger.info("Job scheduler started");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce)
	{
		// Initialize the object container.
		initObjectContainer();
		// Initialize the job scheduler.
		initJobScheduler();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent sce)
	{
		logger.info("Closing mongodb");
		ObjectContainer.getInstance().getMongoDbUtil().getClient().close();
		try
		{
			logger.info("Stopping job scheduler");
			ObjectContainer.getInstance().getJobScheduler().shutdown();
		}
		catch (SchedulerException e)
		{
			logger.error("Scheduler shutdown failed: {}", e);
		}
	}

}
