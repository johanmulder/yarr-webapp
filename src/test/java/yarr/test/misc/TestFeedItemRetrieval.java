/**
 * 
 * File created at 8 apr. 2013 21:32:42 by Johan Mulder <johan@mulder.net>
 */
package yarr.test.misc;

import java.util.Set;

import yarr.app.ObjectContainer;
import yarr.app.web.ContextListener;
import yarr.stor.domain.FeedItem;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class TestFeedItemRetrieval
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		ContextListener c = new ContextListener();
		c.contextInitialized(null);
		ObjectContainer oc = ObjectContainer.getInstance();
		Set<FeedItem> items = oc.getFeedDAOFacade().getFeedItemDAO().getFeedItemsForFeed("5160704d93861277c81d9196", 10);
		System.out.println(items.size());
	}

}
