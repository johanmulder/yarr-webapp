/**
 * 
 * File created at 8 apr. 2013 21:57:55 by Johan Mulder <johan@mulder.net>
 */
package yarr.test.misc;

import java.io.IOException;
import java.util.Set;

import yarr.app.ObjectContainer;
import yarr.app.web.ContextListener;
import yarr.feed.fetch.FeedFetcher;
import yarr.feed.update.FeedItemUpdater;
import yarr.feed.update.FeedUpdater;
import yarr.stor.dao.FeedDAO;
import yarr.stor.dao.FeedItemDAO;
import yarr.stor.domain.Feed;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class TestFeedItemUpdating
{

	/**
	 * @param args
	 * @throws FeedException 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String[] args) throws IllegalArgumentException, IOException, FeedException
	{
		ContextListener c = new ContextListener();
		c.contextInitialized(null);
		ObjectContainer oc = ObjectContainer.getInstance();
		FeedDAO feedDAO = oc.getFeedDAOFacade().getFeedDAO();
		FeedItemDAO feedItemDAO = oc.getFeedDAOFacade().getFeedItemDAO();
		FeedFetcher fetcher = oc.getFeedFetcher();
		FeedUpdater feedUpdater = new FeedUpdater(feedDAO);
		FeedItemUpdater feedItemUpdater = new FeedItemUpdater(feedItemDAO);
		Set<Feed> overdue = feedDAO.getOverdueFeeds();
		for (Feed feed : overdue)
		{
			SyndFeed newFeed = fetcher.fetch(feed.getFeedUrl());
			feedUpdater.updateFromSyndFeed(feed, newFeed);
			feedItemUpdater.updateFromSyndFeed(feed, newFeed);
			
			System.out.println("Feed " + feed.getId() + " (" + feed.getFeedUrl().toExternalForm() + ") is overdue");
		}
		
//		c.contextDestroyed(null);
	}

}
