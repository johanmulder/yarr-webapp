/**
 * 
 * File created at 6 apr. 2013 20:41:16 by Johan Mulder <johan@mulder.net>
 */
package yarr.test.misc;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import yarr.app.ObjectContainer;
import yarr.app.web.ContextListener;
import yarr.feed.fetch.FeedFetcher;
import yarr.feed.update.FeedItemUpdater;
import yarr.feed.update.FeedUpdater;
import yarr.stor.domain.Feed;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;

/**
 * 
 * @author Johan Mulder <johan@mulder.net>
 */
public class TestFeedItemUpdater
{
	public static void main(String[] args) throws IllegalArgumentException, MalformedURLException, IOException, FeedException
	{
		ContextListener c = new ContextListener();
		c.contextInitialized(null);
		ObjectContainer oc = ObjectContainer.getInstance(); 
		FeedFetcher fetcher = oc.getFeedFetcher();
		URL url = new URL("http://www.localhost.nl/~johan/school/Thema_3.1/Project/ars.xml");
		SyndFeed feedData = fetcher.fetch(url);
		Feed feed = oc.getFeedDAOFacade().getFeedDAO().getFeedByFeedUrl(url);
		FeedItemUpdater itemUpdater = new FeedItemUpdater(oc.getFeedDAOFacade().getFeedItemDAO());
		itemUpdater.updateFromSyndFeed(feed, feedData);
		new FeedUpdater(oc.getFeedDAOFacade().getFeedDAO()).updateFromSyndFeed(feed, feedData);
	}
}
